package com.galvanize;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class LightSaberTest {

    @Test
    void ensureLightSaberSerialNumber() {

        LightSaber ls = new LightSaber(12345);

        Assertions.assertEquals(ls.getJediSerialNumber(), 12345);
    }

    @Test
    void checkDefaultColor() {

        LightSaber ls = new LightSaber(12345);

        Assertions.assertEquals(ls.getColor(), "green");
    }

    @Test
    void getFullCharge() {

        LightSaber ls = new LightSaber(12345);

        Assertions.assertEquals(ls.getCharge(), 100.0f);
    }

    @Test
    void checkCharge() {
        LightSaber ls = new LightSaber(22233);
        ls.setCharge(88.2f);

        Assertions.assertEquals(ls.getCharge(), 88.2f);
    }

    @Test
    void checkColor() {
        LightSaber ls = new LightSaber(22233);
        ls.setColor("red");

        Assertions.assertEquals(ls.getColor(), "red");
    }
    @Test
    void checkRecharge() {
        LightSaber ls = new LightSaber(22233);
        ls.setCharge(2.0f);
        ls.recharge();

        Assertions.assertEquals(ls.getCharge(), 100.0f);
    }

    @Test
    void checkUsage() {
        LightSaber ls = new LightSaber(322332);

        ls.use(1.5f);

        Assertions.assertEquals(ls.getCharge(),99.75f);

    }

    @Test
    void checkMinutesRemaining() {
        LightSaber ls = new LightSaber(322339);

        ls.use(1.5f);

        Assertions.assertEquals(ls.getRemainingMinutes(), 299.25);
    }



}
